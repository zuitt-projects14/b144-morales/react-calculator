import React from "react"
import { Container, Form, Button } from 'react-bootstrap';
import { useState, useEffect, } from 'react';
import { Row, Col, Card } from "react-bootstrap";


export default function CalcComp(){

const [input1, setInput1] = useState("")
const [input2, setInput2] = useState("")
const [result, setResult] = useState(0)

const reset = () => {
    setResult(0);
    setInput1("");
    setInput2("");
}


	return(
<>


<Row className = "mt-3 mb-3">
      
      
      <Col xs={12} md={8} className="mx-auto">
    <Card className="cardHighlight p-3">
  <Card.Body>

<Container>

<h1 className="text-center">Calculator</h1>
<div className="result">
                <h1 className="text-center" value={result}>{result}</h1>
            </div>

                <Form>
                    
                    <Form.Group controlId="input1">
                        <Form.Label></Form.Label>
                        <Form.Control 
        	                type="Number" 
                            value={input1} 
                            onChange={e => setInput1(e.target.value)}
        	                required
                        />
                    </Form.Group>

                    <Form.Group controlId="Input2">
                        <Form.Label></Form.Label>
                        <Form.Control 
        	                type="Number" 
                            value={input2} 
                            onChange={e => setInput2(e.target.value)}
        	                required
                        />
                    </Form.Group>
                        </Form>

                        <Button variant="danger" id="add" className="m-3" onClick={(e)=> setResult( Number(input1) + Number(input2))}>
                            Add
                        </Button> 
                        <Button variant="danger" id="subtract" className="m-3" onClick={(e)=> setResult( Number(input1) - Number(input2))}>
                            Subtract
                        </Button>
                        <Button variant="danger" id="mulitply" className="m-3" onClick={(e)=> setResult( Number(input1) * Number(input2))}>
                            Multiple
                        </Button>
                        <Button variant="danger" id="divide" className="m-3" onClick={(e)=> setResult( Number(input1) / Number(input2))}>
                             Divide
                        </Button>
                        <Button variant="danger" id="reset" className="m-3" onClick={()=> reset()}>
                            Reset
                        </Button>
                        
                
            </Container>

              </Card.Body>  
</Card>   
      </Col>
    </Row>
</>
		)
}